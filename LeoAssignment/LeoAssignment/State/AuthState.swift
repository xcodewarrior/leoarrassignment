//
//  AuthState.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/7/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import ReSwift

struct AuthState: StateType, AuthStateEssential {
    func get() -> String {
        return self.authUserName
    }
    
    mutating func set(_ with: String) {
        self.authUserName = with
    }
    
    typealias AuthStoreType = String
    
    var authUserName: String
}

protocol AuthStateEssential {
    associatedtype AuthStoreType
    
    var authUserName: AuthStoreType { get }
    
    func get() -> AuthStoreType
    
    mutating func set(_ with: AuthStoreType)
}
