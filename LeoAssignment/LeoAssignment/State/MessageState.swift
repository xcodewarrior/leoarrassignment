//
//  MessageState.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/6/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import ReSwift

struct MessageState: StateType, MessageStateEssential {
    typealias MessageType = String
    
    var messages: [String] = []
}

protocol MessageStateEssential {
    associatedtype MessageType
    
    var messages: [MessageType] { get }
}
