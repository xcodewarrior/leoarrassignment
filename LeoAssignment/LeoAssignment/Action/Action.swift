//
//  Action.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/6/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import ReSwift

// Action for message store

struct AddMessageAction: Action {
    let newMessage: String
}

struct ReplaceMessageAction: Action {
    let newMessages: [String]
}

struct CleanMessageAction: Action {}


// Action for user handle

struct SetAuthAction: Action {
    let authUserName: String
}

struct CleanAuthAction: Action {}
