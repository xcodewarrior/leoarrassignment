//
//  MistyIndicator.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/6/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class MistyIndicator: UIView {
    
    private var indicator: UIActivityIndicatorView = {
        var indi = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        indi.color = UIColor.facebookBubbleColor
        indi.translatesAutoresizingMaskIntoConstraints = false
        indi.startAnimating()
        return indi
    }()
    
    
    private func setUI() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 6.0
        backgroundColor = UIColor.lightGray
        
        
        addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        indicator.heightAnchor.constraint(equalToConstant: 40).isActive = true
        indicator.widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        
        setUI()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

