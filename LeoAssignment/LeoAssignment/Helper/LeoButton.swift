//
//  LeoButton.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/6/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class LeoButton: UIButton {
    
    public func setUIForLobby() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 7
        backgroundColor = UIColor.blue
        setTitleColor(UIColor.white, for: UIControl.State.normal)
        setTitleColor(UIColor.lightGray, for: UIControl.State.highlighted)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
