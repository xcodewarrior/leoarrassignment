//
//  InputView.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/7/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class InputView: UIView {
    
    weak var messagerDelegate: MessageProtocol?
    
    
    var inputTextField: LeoField!
    var dismissButton: UIButton!
    var sendButton: UIButton!
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUI() {
        
       translatesAutoresizingMaskIntoConstraints = false
        
        backgroundColor = UIColor.white
        
        inputTextField = LeoField()
        inputTextField.placeholder = "Send a message.."
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 40))
        inputTextField.leftView = leftView
        inputTextField.leftViewMode = UITextField.ViewMode.always
        
        addSubview(inputTextField)
        
        inputTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: 2).isActive = true
        inputTextField.topAnchor.constraint(equalTo: topAnchor, constant: 1).isActive = true
        inputTextField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1).isActive = true
        inputTextField.rightAnchor.constraint(equalTo: rightAnchor,constant: -60).isActive = true
        
        dismissButton = UIButton()
        dismissButton.setImage(UIImage(named:"downButton")!, for: UIControl.State.normal)
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.addTarget(self, action: #selector(InputView.dismiss(_:)), for: UIControl.Event.touchUpInside)
        
        addSubview(dismissButton)
        dismissButton.heightAnchor.constraint(equalToConstant: 27).isActive = true
        dismissButton.widthAnchor.constraint(equalToConstant: 27).isActive = true
        dismissButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        dismissButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        sendButton = UIButton()
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.setImage(UIImage(named: "sendButton")!, for: UIControl.State.normal)
        sendButton.addTarget(self, action: #selector(InputView.sendMessage(_:)), for: UIControl.Event.touchUpInside)
        addSubview(sendButton)
        
        sendButton.rightAnchor.constraint(equalTo: dismissButton.leftAnchor, constant: -1).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: 27).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 27).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }
    
    @objc private func dismiss(_ sender: UIButton) {
        messagerDelegate?.dismiss()
    }
    
    @objc private func sendMessage(_ sender: UIButton) {
        guard let message = inputTextField.text else { return }
        messagerDelegate?.sendMessage(message)
    }

}
