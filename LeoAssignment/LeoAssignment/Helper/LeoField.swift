//
//  LeoField.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/6/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class LeoField: UITextField, Base {
    
    func animate() {
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position.x"
        animation.values = [0,10,-10,10,0]
        animation.keyTimes = [0,1/6.0,3/6.0,5/6.0,1] as [NSNumber]
        animation.duration = 0.4
        animation.isAdditive = true
        animation.isRemovedOnCompletion = true
        
        let borderAnimation = CABasicAnimation()
        borderAnimation.keyPath = "borderColor"
        borderAnimation.fromValue = UIColor.lightGray.cgColor
        borderAnimation.toValue = UIColor.red.cgColor
        borderAnimation.duration = 1.2
        
        let borderWidthAnimation = CABasicAnimation()
        borderWidthAnimation.keyPath = "borderWidth"
        borderWidthAnimation.fromValue = 1.5
        borderWidthAnimation.toValue = 3.5
        borderWidthAnimation.duration = 1.2
        
        let group = CAAnimationGroup()
        group.animations = [
            animation,
            borderAnimation,
            borderWidthAnimation
        ]
        self.layer.add(group, forKey: nil)
        
    }
    
    func validAnimation() {
        let borderColor = CABasicAnimation()
        borderColor.keyPath = "borderColor"
        borderColor.fromValue = UIColor.lightGray.cgColor
        borderColor.toValue = UIColor.green.cgColor
        borderColor.duration = 0.4
        self.layer.add(borderColor, forKey: "borderColor")
        
        let borderWidth = CABasicAnimation()
        borderWidth.keyPath = "borderWidth"
        borderWidth.fromValue = 1.0
        borderWidth.toValue = 2.5
        borderWidth.duration = 0.4
        self.layer.add(borderWidth, forKey: "borderWidth")
    }
    
    func validate() -> Bool {
        guard let txt = self.text else { return false }
        
        return txt.count > 2
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUI()
    }
    
    internal func setUI() {
        self.placeholder = "Username - At least 2 characters"
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(image: UIImage(named: "user"), highlightedImage: nil)
        imageView.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.center = leftView.center
        leftView.addSubview(imageView)
        
        self.leftView = leftView
        leftViewMode = UITextField.ViewMode.always
        
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 4
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.lightGray.cgColor
        font = UIFont.getMessageFont()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
