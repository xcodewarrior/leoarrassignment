//
//  FirebaseHandler.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/6/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation

import Firebase
import FirebaseDatabase
import FirebaseCore

final class FirebaseHandler {
    
    public static var shared: FirebaseHandler = FirebaseHandler()
    
    private init() { }
    
    
    public func addMessage(_ withUser: String, _ withMessage: String ) {
        let databaseReference = Database.database().reference()
        var messageProp: String = "messages"
        databaseReference.child("users").child(withUser).observeSingleEvent(of: DataEventType.value,
                                                                            with: { (snapshot) in
                                                                                
                                                                                let value = snapshot.value as? NSDictionary
                                                                                
                                                                                if let sValue = value {
                                                                                    
                                                                                    print("NOT NIL VALE-UE -> ", sValue)
                                                                                    
                                                                                    if let _messages = sValue["messages"] as? [String] {
                                                                                        print("Array -> ", _messages)
                                                                                        
                                                                                        var newArray = _messages
                                                                                        newArray.append(withMessage)
                                                                                        
                                                                                        databaseReference.child("users").child(withUser).updateChildValues([messageProp:newArray])
                                                                                        
                                                                                    }
                                                                                    
                                                                                } else {
                                                                                    
                                                                                    print("Value is nil ")
                                                                                    databaseReference.child("users").child(withUser).child(messageProp).setValue([withMessage])
                                                                                    
                                                                                    
                                                                                }
                                                                                
        }) { (error) in
            
            print("Error -> ", error)
            
        }
    }
    
    public func getMessages(_ ofUser: String, _ completionHandler: @escaping (_ withArray: [String]) -> ()) {
        let databaseReference = Database.database().reference()
        
        databaseReference.child("users").child(ofUser).observe(DataEventType.value) { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            guard let generalValue = value as? NSDictionary else {
                completionHandler([])
                return
            }
            
            guard let messagesArray = generalValue["messages"] as? [String] else {
                completionHandler([])
                return
            }
            
            completionHandler(messagesArray)
                    
        }
    }
    
}
