//
//  ChatCoordinator.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit


final class ChatCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator]
    var currentViewController: ChatController
    var navigationController: UINavigationController
    var parentCoordinator: LobbyCoordinator?
    
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.currentViewController = ChatController()
        self.childCoordinators = [Coordinator]()
        self.currentViewController.coordinator = self
    }
    
    func start() {
        
        self.navigationController.pushViewController(self.currentViewController,
                                                     animated: true)
    }
    
    
    func popBack() {
        self.parentCoordinator?.popFromChatController()
    }
    
    
    

    
}
