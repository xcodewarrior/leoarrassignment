//
//  Coordinator.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

protocol Coordinator {
    
    var childCoordinators: [Coordinator] { get }
    
    var navigationController: UINavigationController { get }
    
    init(with navigationController: UINavigationController)
    
    func start()
    
}
