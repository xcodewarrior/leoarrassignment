//
//  SplashCoordinator.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class SplashCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator]
    var currentViewController: SplashController
    var navigationController: UINavigationController
    
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.currentViewController = SplashController()
        self.childCoordinators = [Coordinator]()
        self.currentViewController.coordinator = self
    }
    
    func start() {
        self.navigationController.pushViewController(currentViewController,
                                                     animated: true)
    }
    
    
    func log() {
        print("loggy")
    }
    
    
}
