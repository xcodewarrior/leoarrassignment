//
//  LobbyCoordinator.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class LobbyCoordinator: Coordinator {
    
    var currentViewController: LobbyController
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator]
    
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.currentViewController = LobbyController()
        self.childCoordinators = [Coordinator]()
        self.currentViewController.coordinator = self
    }
    
    func start() {
        currentViewController.viewModel = LobbyViewModel("", "Lobby")
        navigationController.pushViewController(self.currentViewController,
                                                animated: true)
    }
    
    func openChatController(_ withName: String, _ withMessages: [String]) {
        
        let chatCoordinator = ChatCoordinator(with: self.navigationController)
        chatCoordinator.currentViewController.viewModel = ChatViewModel(withName)
        chatCoordinator.parentCoordinator = self
        chatCoordinator.start()
        self.childCoordinators.append(chatCoordinator)
        
        FirebaseHandler.shared.getMessages(withName) { (messages) in
            chatCoordinator.currentViewController.viewModel = ChatViewModel(withName, messages)
        }

    }
    
    func popFromChatController() {
        
        self.navigationController.popViewController(animated: true)
        
        if !childCoordinators.isEmpty {
            childCoordinators.removeLast()
        }

    }
    
}
