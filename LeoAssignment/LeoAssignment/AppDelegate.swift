//
//  AppDelegate.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import ReSwift

let mainStore = Store<MessageState>(
    reducer: messageReducer,
    state: nil)

let authStore = Store<AuthState>(
    reducer: authReducer,
    state: nil)


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var splashNavigationController: UINavigationController?
    var mainNavigationController: UINavigationController?
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        FirebaseApp.configure()

        if let val = DefaultsManager.shared.get() {
            scenarioForSingleUser(with: val)
        } else {
            prepareSplash()
        }
        
        
        return true
    }
    
    
    
    internal func prepareSplash() {
        splashNavigationController = UINavigationController()
        let appCoordinator = SplashCoordinator(with: splashNavigationController ?? UINavigationController())
        window?.rootViewController = splashNavigationController
        appCoordinator.start()
    }

    internal func prepareApp() {
        mainNavigationController = UINavigationController()
        let appCoordinator = LobbyCoordinator(with: mainNavigationController ?? UINavigationController())
        window?.rootViewController = mainNavigationController
        appCoordinator.start()
    }
    
    private func prepareChatApp() {
        mainNavigationController = UINavigationController()
        let chatCoordinator = ChatCoordinator(with: mainNavigationController ?? UINavigationController())
        window?.rootViewController = mainNavigationController
        chatCoordinator.start()
    }
    
    private func scenarioForSingleUser(with user: String) {
        mainNavigationController = UINavigationController()
        let appCoordinator = LobbyCoordinator(with: mainNavigationController ?? UINavigationController())
        window?.rootViewController = mainNavigationController
        appCoordinator.start()
        appCoordinator.openChatController(user, [])
    }
    
    
}

//https://jsonblob.com/api/jsonBlob/4f421a10-5c4d-11e9-8840-0b16defc864d.
