//
//  ChatViewModel.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation


final class ChatViewModel: ChatViewModelEssential {
    typealias ValueType = String
    
    var titleString: ValueType
    var messages: [ValueType]
    
    
    init(_ titleString: String) {
        self.titleString = titleString
        self.messages = []
    }
    
    init(_ titleString: String, _ messages: [ValueType]) {
        self.titleString = titleString
        self.messages = messages
    }
    
}

protocol ChatViewModelEssential {
    associatedtype ValueType
    
    var titleString: ValueType { get }
    var messages: [ValueType] { get }
    
    
}
