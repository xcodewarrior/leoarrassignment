//
//  LobbyViewModel.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation


final class LobbyViewModel: LobbyViewModelEssentials {
    
    typealias ValueType = String
    
    var lobbyUserName: ValueType
    var navigationTitle: ValueType
    
    init(_ lobbyUserName: ValueType, _ navTitle: ValueType) {
        self.lobbyUserName = lobbyUserName
        self.navigationTitle = navTitle
    }
    
    func updateLobbyName(_ withName: String) {
        self.lobbyUserName = withName
    }
    
}

protocol LobbyViewModelEssentials {
    associatedtype ValueType
    
    var lobbyUserName: ValueType { get }
    var navigationTitle: ValueType { get }
    func updateLobbyName(_ withName: ValueType)

}
