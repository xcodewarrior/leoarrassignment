//
//  StringExtension.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

extension String {
    
    func getRectSize(with initialSize: CGSize?) -> CGRect {
        
        let _size = initialSize ?? CGSize(width: 250, height: 10000)
        
        return (self as NSString).boundingRect(with: _size ,
                                               options: NSStringDrawingOptions.usesLineFragmentOrigin.union(NSStringDrawingOptions.usesFontLeading),
                                               attributes: [NSAttributedString.Key.font: UIFont.getMessageFont()],
                                               context: nil)
        
    }
    
    var isComfortableForUsername: Bool {
        
        if self.count > 2 {
            return true
        }
        
        return false
    }
    
}
