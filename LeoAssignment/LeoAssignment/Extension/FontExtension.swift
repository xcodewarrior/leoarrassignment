//
//  FontExtension.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/4/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

extension UIFont {
    
    public class func getMessageFont() -> UIFont {
        return UIFont(name: "SegoeUI", size: 15)!
    }
    
    public class func getThinFont() -> UIFont {
        return UIFont(name: "SegoeUI", size: 13)!
    }
    
    public class func getNavFont() -> UIFont {
        return UIFont(name: "SegoeUI", size: 18)!
    }
    
}
