//
//  UIColorExtension.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/4/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

extension UIColor {
    
    public class var lightWhite: UIColor {
        return UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
    }
    
    public class var facebookBubbleColor: UIColor {
        return UIColor(red: 0, green: 120/255, blue: 255/255, alpha: 1)
    }
    
}
