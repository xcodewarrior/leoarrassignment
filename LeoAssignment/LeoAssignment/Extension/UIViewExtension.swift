//
//  UIViewExtension.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/4/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit


extension UIView {
    
    func presentInWindow(_ with: UIView?) {
        
        guard let withView = with else { return }
        
        DispatchQueue.main.async {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let window = appDelegate.window as! UIWindow
            
            window.addSubview(withView)
            withView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            withView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
            withView.heightAnchor.constraint(equalToConstant: 80).isActive = true
            withView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        }
    }

    
}

