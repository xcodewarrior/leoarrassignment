//
//  Manager.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/7/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation

protocol Manager {
    associatedtype HandleType
    
    func set(_ withType: HandleType)
    
    func get() -> HandleType?

}
