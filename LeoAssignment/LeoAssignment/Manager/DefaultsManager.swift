//
//  DefaultsManager.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/7/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation
import UIKit

final class DefaultsManager: Manager {

    func set(_ withType: String) {
        
        UserDefaults.standard.set(withType, forKey: authKey)
        UserDefaults.standard.synchronize()
    }
    
    
    func get() -> String? {
        
        if let auth = UserDefaults.standard.string(forKey: authKey) {
            return auth
        }
        
        return nil
    }
    
    typealias HandleType = String
    
    
    private let authKey = "auth"
    
    static let shared: DefaultsManager = DefaultsManager()
    
    private init() {}
}
