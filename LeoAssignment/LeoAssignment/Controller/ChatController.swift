//
//  ChatController.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit
import ReSwift

final class ChatController: UIViewController, Base, StoreSubscriber {
    
    var coordinator: ChatCoordinator?
    
    var viewModel: ChatViewModel? {
        didSet {
            self.updateUI()
        }
    }
     
    var messagesArray: [String] = []
    
    var inputTextField: UITextField = {
        var textField: UITextField = UITextField()
        textField.placeholder = "enter input"
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = UIColor.red
        return textField
    }()
    
    var inputBottomConstraint: NSLayoutConstraint!
    
    private lazy var chatCollectionView: UICollectionView = {
        let collectionViewLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        
        var collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.register(ResponseCell.self, forCellWithReuseIdentifier: "ResponseCell")
        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 40, right: 0)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = UIColor.white
        collectionView.dataSource = self
        collectionView.delegate = self
        return collectionView
    }()
    
    private lazy var inputField: InputView = {
        var field: InputView = InputView(frame: CGRect.zero)
        field.translatesAutoresizingMaskIntoConstraints = false
        field.messagerDelegate = self
        return field
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        setUI()

    }
    
    
    @objc func testFunc(_ sender: UIBarButtonItem) {
        guard let str = inputTextField.text else { return }
        
        self.sendMessage(str)
    }
    
    
    private func sendMessageViaFirebase(with message: String) {
        
        guard let viewModel = self.viewModel else { return }
        
        if message.count != 0 {
            
            print("mesage -> ", message)
            FirebaseHandler.shared.addMessage(viewModel.titleString, message)
        }
    }
    

    
    internal func setUI() {
        view.backgroundColor = UIColor.lightGray
        
        view.addSubview(chatCollectionView)
        chatCollectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        chatCollectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        chatCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        chatCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        view.addSubview(inputField)
        
        inputField.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        inputField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        inputField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        inputBottomConstraint = NSLayoutConstraint(item: inputField,
                                                   attribute: NSLayoutConstraint.Attribute.bottom,
                                                   relatedBy: NSLayoutConstraint.Relation.equal,
                                                   toItem: self.view.safeAreaLayoutGuide,
                                                   attribute: NSLayoutConstraint.Attribute.bottom,
                                                   multiplier: 1.0,
                                                   constant: 0)
        inputBottomConstraint.isActive = true
        
        
        let popBarButton = UIBarButtonItem(image: UIImage(named:"backIco"),
                                           style: UIBarButtonItem.Style.done,
                                           target: self,
                                           action: #selector(ChatController.popBack(_:)))
        popBarButton.tintColor = UIColor.black
        navigationItem.leftBarButtonItem = popBarButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mainStore.subscribe(self)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatController.keyBoardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ sender: Notification) {
        
        if let keyboardFrame: NSValue = sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            var padding: CGFloat = 0.0
            
            if #available(iOS 11.0, *) {
                
                let window = UIApplication.shared.keyWindow
                let bottomPadding = window?.safeAreaInsets.bottom
                padding = bottomPadding ?? 0.0
   
            }
            
            
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.inputBottomConstraint.constant -= (keyboardHeight - padding)
                self.view.layoutIfNeeded()
            }
            
        }
        
    }
    
    @objc private func keyBoardWillHide(_ sender: Notification) {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.inputBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        mainStore.unsubscribe(self)
        authStore.unsubscribe(self)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    
    func newState(state: MessageState) {
        print("NEW STATE")
        
        DispatchQueue.main.async { [unowned self] in
            self.chatCollectionView.reloadData()
            self.chatCollectionView.scrollToItem(at: IndexPath(row: self.messagesArray.count-1, section: 0),
                                                 at: UICollectionView.ScrollPosition.bottom,
                                                 animated: true)
        }
        
    }
    
    private func replaceMessages() {
        
        guard let viewModel = viewModel else { return }
        
        mainStore.dispatch(
            ReplaceMessageAction(newMessages: viewModel.messages)
        )
        
    }
    
    @objc private func addMessage(_ sender: UIButton) {
        
        // it can be improved
        let message: String = ""
        
        mainStore.dispatch(
            AddMessageAction(newMessage: message)
        )
    }
    
    @objc private func clearMessage(_ sender: UIButton) {
        
        mainStore.dispatch(
            CleanMessageAction()
        )
    }
    
    
    @objc private func popBack(_ sender: UIBarButtonItem) {
        
        self.coordinator?.popBack()
        
    }
    
}


extension ChatController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewModel = self.viewModel else { return 0 }
        return viewModel.messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let viewModel = self.viewModel else { return UICollectionViewCell() }
        
        let message = messagesArray[indexPath.row]
        
        let rect = message.getRectSize(with: nil)
        
        guard let responseCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResponseCell", for: indexPath) as? ResponseCell else {
            return UICollectionViewCell()
        }
        
        if ( indexPath.row % 3 == 0 ) {
            
            responseCell.bubbleView.frame = CGRect(x: self.view.frame.width - rect.width - 30, y: 0, width: rect.width + 10 + 8, height: rect.height + 20)
            responseCell.chatText.frame = CGRect(x: 4, y: 0, width: rect.width + 10 , height: rect.height + 20)
            responseCell.chatTextProperty = message
            responseCell.isFirstCell = false
            
            
        } else {
            
            responseCell.bubbleView.frame = CGRect(x: 12, y: 0, width: rect.width + 10 + 8, height: rect.height + 20)
            responseCell.chatText.frame = CGRect(x: 0, y: 0, width: rect.width + 10 , height: rect.height + 20)
            responseCell.chatTextProperty = message
            responseCell.profileNameProperty = viewModel.titleString
            responseCell.isFirstCell = true
            
        }

        return responseCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if messagesArray.count == 0 {
            return CGSize(width: self.view.frame.width,
                          height: self.view.frame.height)
        }
        
        
        let message = messagesArray[indexPath.row]
        
        let rect = message.getRectSize(with: nil)
        
        let avatarHeight: CGFloat = 50.0
        
        return CGSize(width: self.view.frame.width, height: rect.height + 20 + avatarHeight)
    }
    
    func updateUI() {
        
        guard let viewModel = self.viewModel else { return }
        
        navigationItem.title = viewModel.titleString
        self.messagesArray = viewModel.messages
        self.replaceMessages()
        
    }
    
}


protocol MessageProtocol: class {
    func sendMessage(_ withMessage: String)
    func dismiss()
}

extension ChatController: MessageProtocol{
    func sendMessage(_ withMessage: String) {
        self.sendMessageViaFirebase(with: withMessage)
    }
    
    func dismiss() {
        self.view.endEditing(true)
    }
}
