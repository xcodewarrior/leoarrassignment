//
//  Base.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation


protocol Base {
    
    func setUI()
    
}
