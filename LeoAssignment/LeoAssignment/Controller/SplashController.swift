//
//  SplashController.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class SplashController: UIViewController, Base {
    
    var coordinator: SplashCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setUI()
        setTrigger()
        
    }
    
    private var iconImageView: UIImageView = {
        var imageView: UIImageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "leoarr")
        imageView.layer.cornerRadius = 100
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private func getIntoApp() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.prepareApp()
        
    }
    
    internal func setUI() {
        view.backgroundColor = UIColor.lightWhite
        
        view.addSubview(iconImageView)
        iconImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iconImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        guard let navController = self.navigationController else { return }
        navController.navigationBar.isHidden = true
    }
    
    private func setTrigger() {
        UIView.animate(withDuration: 0.8,
                       animations: { [unowned self] in
                        self.view.backgroundColor = UIColor(white: 1.0, alpha: 0.6)
        }) { _ in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) { [unowned self] in
                self.getIntoApp()
            }
        }
    }
    
    
    
}
