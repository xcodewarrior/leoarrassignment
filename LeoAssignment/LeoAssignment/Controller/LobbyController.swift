//
//  LobbyController.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseCore
import ReSwift


final class LobbyController: UIViewController, Base, StoreSubscriber {
    
    var coordinator: LobbyCoordinator?
    var reference: DatabaseReference!
    var indicator: MistyIndicator?
    
    
    var viewModel: LobbyViewModel? {
        didSet {
            self.updateUI()
        }
    }
    
    private var usernameTextField: LeoField = {
        var textField: LeoField = LeoField()
        return textField
    }()
    
    private lazy var signButton: LeoButton = {
        let button = LeoButton()
        button.setUIForLobby()
        button.setTitle("JOIN ROOM", for: UIControl.State.normal)
        button.addTarget(self, action: #selector(LobbyController.openChatController(_:)), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUI()
        
        
        
        self.view.presentInWindow(indicator)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) { [unowned self] in
            
            self.indicator?.removeFromSuperview()
            self.indicator = nil
            
        }

    }
    
    
    internal func setUI() {
        view.backgroundColor = UIColor.lightWhite
        
        
        view.addSubview(signButton)
        signButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -50).isActive = true
        signButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        signButton.widthAnchor.constraint(equalToConstant: 180).isActive = true
        
        view.addSubview(usernameTextField)
        usernameTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        usernameTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        usernameTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        usernameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        usernameTextField.bottomAnchor.constraint(equalTo: signButton.topAnchor, constant: -20).isActive = true
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightWhite
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.getNavFont()
        ]
        
        indicator = MistyIndicator()
    }
    
    
    @objc private func openChatController(_ sender: UIButton) {
    
        guard let username = self.usernameTextField.text else { return }
        guard let viewModel = self.viewModel else { return }
        guard let coordinator = self.coordinator else { return }
        
        
        authStore.dispatch(
            SetAuthAction(authUserName: username)
        )
        
        viewModel.updateLobbyName(username)
        
        if !usernameTextField.validate() {
            usernameTextField.animate()
        } else {
            usernameTextField.validAnimation()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {
                coordinator.openChatController(viewModel.lobbyUserName, [])
            }
        }
        
    }
    
    
    private func updateUI() {
        
        guard let viewModel = self.viewModel else { return }
        navigationItem.title = viewModel.navigationTitle
        print("lobby name -> ", viewModel.lobbyUserName)
    }
    
    func newState(state: AuthState) {
        print("New state, new user namme -> ", state.authUserName)
        
        if state.authUserName.isComfortableForUsername {
            
            DefaultsManager.shared.set(state.authUserName)
            
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        authStore.subscribe(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        authStore.unsubscribe(self)
    }
    
}
