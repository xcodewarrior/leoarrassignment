//
//  ResponseCell.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/3/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import UIKit

final class ResponseCell: UICollectionViewCell, ResponseHandler {
    
    typealias PropertyType = String
    
    
    var chatTextProperty: String = "" {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.chatText.text = self.chatTextProperty
            }
        }
    }
    
    var profileNameProperty: String = "" {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.profileNameLabel.text = self.profileNameProperty
            }
        }
    }
    
    public var bubbleView: UIView = {
        var view: UIView = UIView()
        view.backgroundColor = UIColor.facebookBubbleColor
        view.layer.cornerRadius = 3
        return view
    }()
    
    public var chatText: UITextView = {
        var textView = UITextView()
        textView.textColor = UIColor.white
        textView.font = UIFont.getMessageFont()
        textView.backgroundColor = UIColor.clear
        textView.isEditable = false
        return textView
    }()
    
    private var profileImageView: UIImageView = {
        var imageView: UIImageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.image = UIImage(named:"man")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 12.5
        return imageView
    }()
    
    private var profileNameLabel: UILabel = {
        var label:UILabel = UILabel()
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Label"
        label.font = UIFont.getMessageFont()
        return label
    }()
    
    private var stickImageView: UIImageView = {
        var imageView: UIImageView = UIImageView()
        imageView.image = UIImage(named:"bubbleStick")!
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    var isFirstCell: Bool = false {
        didSet{
            
            if isFirstCell {
                stickConstraint.constant = 20
                profileImageConstraint.constant = 10
                self.layoutIfNeeded()
            } else {
                stickConstraint.constant = bubbleView.frame.width - 20
                profileImageConstraint.constant = self.frame.width - 90
                profileNameProperty = "BOT"
                self.layoutIfNeeded()
            }
            
        }
    }
    
    var stickConstraint: NSLayoutConstraint!
    var profileImageConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(bubbleView)
        bubbleView.addSubview(chatText)
        
        addSubview(profileImageView)
        profileImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
        profileImageView.topAnchor.constraint(equalTo: bubbleView.bottomAnchor, constant: 10).isActive = true
        
        profileImageConstraint = NSLayoutConstraint(item: profileImageView,
                                                    attribute: NSLayoutConstraint.Attribute.left,
                                                    relatedBy: NSLayoutConstraint.Relation.equal,
                                                    toItem: self,
                                                    attribute: NSLayoutConstraint.Attribute.left,
                                                    multiplier: 1.0,
                                                    constant: 10)
        profileImageConstraint.isActive = true
    
        
        addSubview(profileNameLabel)
        profileNameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 10).isActive = true
        profileNameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
        
        addSubview(stickImageView)
        stickImageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        stickImageView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        stickImageView.topAnchor.constraint(equalTo: bubbleView.bottomAnchor, constant: -1).isActive = true
        
        stickConstraint = NSLayoutConstraint(item: stickImageView,
                                            attribute: NSLayoutConstraint.Attribute.left,
                                            relatedBy: NSLayoutConstraint.Relation.equal,
                                            toItem: self.bubbleView,
                                            attribute: NSLayoutConstraint.Attribute.left,
                                            multiplier: 1.0,
                                            constant: 10)
        stickConstraint.isActive = true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    
}


protocol ResponseHandler {
    associatedtype PropertyType
    
    var chatTextProperty: PropertyType { get }
    var profileNameProperty: PropertyType { get }
}
