//
//  Dispatchers.swift
//  LeoAssignment
//
//  Created by Emrah Korkmaz on 9/6/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import ReSwift

func messageReducer(action: Action, state: MessageState?) -> MessageState {
    
    var state = state ?? MessageState()
    
    switch action {
    
    case let messageAction as AddMessageAction:
        state.messages.append(messageAction.newMessage)
        break
    case let replaceAction as ReplaceMessageAction:
        state.messages = replaceAction.newMessages
    case _ as CleanMessageAction:
        state.messages = []
        break
    default:
        break
    }
    
    return state
}


func authReducer(action: Action, state: AuthState?) -> AuthState {
    
    var state = state ?? AuthState(authUserName: "")
    
    switch action {
    case let authAction as SetAuthAction:
        state.authUserName = authAction.authUserName
    case _ as CleanAuthAction:
        state.authUserName = ""
    default:
        break
    }
    
    
    return state
}
